clear
reset
set key off
set border 3
set xrange [780:790]
#set terminal eps nocrop enhanced size 1366,768
#set output 'data_2.png'

set term postscript eps enhanced 24

set output 'data_2_.eps'
set xlabel 'x'
set ylabel 'frequency'
set xtics 1
# Add a vertical dotted line at x=0 to show centre (mean) of distribution.
set yzeroaxis

# Each bar is half the (visual) width of its x-range.
set boxwidth 0.9 absolute
set style fill solid 1.0 noborder

set style fill pattern 2
set style histogram columnstacked

bin_width = 1;

bin_number(x) = floor(x/bin_width)

rounded(x) = bin_width * ( bin_number(x) + 0 )

unset label
set title "Estimation of pi"
plot 'frequencies' i 0 using (rounded($1)):(1.0) smooth frequency with boxes
