a = zeros(1000,2000)

a = unifrnd(-1,1,1000,2000);
save samples a

count = zeros(1000,1);

for i = 1:1000
	for j = 1:1000
		if((a(j,2*i-1)*a(j,2*i-1) + a(j,2*i)*a(j,2*i)) <= 1 )
			count(i) = count(i) + 1;
		endif
	endfor
endfor
save frequencies count